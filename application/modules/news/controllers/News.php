<?php
class News extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
{
	$data['news'] = $this->news_model->get_news();
	$data['title'] = 'Новости' . " | " . $this->config->item('title');

	$this->load->view('templates/header', $data);
	$this->load->view('news/index', $data);
	$this->load->view('templates/footer');
}

	public function view($link)
{
	$data['news_item'] = $this->news_model->get_news($link);

	if (empty($data['news_item']))
	{
		show_404();
	}

	$data['title'] = $data['news_item']['title'];

	$this->load->view('templates/header', $data);
	$this->load->view('news/view', $data);
	$this->load->view('templates/footer');
}

public function add()
{
	$this->load->helper('form');
	$this->load->library('form_validation');

	$data['title'] = 'Добавить новость' . " | " . $this->config->item('title');

	$this->form_validation->set_rules('title', 'Title', 'required');
	$this->form_validation->set_rules('text', 'text', 'required');

	if ($this->form_validation->run() === FALSE)
	{
		$this->load->view('templates/header', $data);
		$this->load->view('news/add');
		$this->load->view('templates/footer');

	}
	else
	{
		$this->news_model->set_news();
		$this->load->view('news/success');
	}
}

public function del($id)
{
    
    if (!$this->ion_auth->is_admin()){
        echo "Ошибка доступа";
        echo '<META HTTP-EQUIV="REFRESH" CONTENT="3;URL=' . base_url("/auth") . '">';
    }else{
	$this->news_model->del_news($id);
    echo '<META HTTP-EQUIV="REFRESH" CONTENT="3;URL=' . base_url("/news") . '">';}
}
}