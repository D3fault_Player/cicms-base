<?php
class News_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
    public function get_news($link = FALSE)
{
	if ($link === FALSE)
	{
		$query = $this->db->get('news');
		return $query->result_array();
	}

	$query = $this->db->get_where('news', array('link' => $link));
	return $query->row_array();
}
public function set_news()
{
	$this->load->helper('url');
    $this->load->library('Translit');

$Translit = new Translit;
	$link = url_title($Translit->forLink($this->input->post('title')), 'dash', TRUE);

	$data = array(
		'title' => $this->input->post('title'),
		'link' => $link,
		'text' => $this->input->post('text')
	);

	return $this->db->insert('news', $data);
}
// TODO: сделать удаление новостей из базы.

  public function del_news($id)
  {
    $this->db->where('id', $id);    
  	$this->db->delete('news');
 }

}