<?php foreach ($news as $news_item): ?>

    <h2><?php echo $news_item['title'] ?></h2>
    <div class="main">
        <?php echo $news_item['text'] ?>
    </div>
    <p><a href="/news/<?php echo $news_item['link'] ?>">Показать статью</a></p>
    <p><a href="/news/delete/<?php echo $news_item['id']?>">Удалить статью</a></p>

<?php endforeach ?>