<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//News module

$route['news/add'] = 'news/add';
$route['news/delete/(:any)'] = 'news/del/$1';
$route['news/(:any)'] = 'news/view/$1';
$route['news'] = 'news';