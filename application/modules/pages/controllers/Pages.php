<?php

class Pages extends MX_Controller
{

    public function __construct()
    {
        $this->load->database();

    }
    public function view($page = 'main')
    {

        if (!file_exists(APPPATH . 'modules/pages/views/' . $page . '.php')) {
            show_404();
        }else{
            $pagename = $this->db->query("SELECT * FROM pages WHERE name='$page' LIMIT 1");
        $result = $pagename->row();
        }
        
        $data['title'] = ucfirst($result->description) . " | " . $this->config->item('title');
 
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }

}
